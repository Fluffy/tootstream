# tootstream
Tootstream est un client [Mastodon](https://fr.wikipedia.org/wiki/Mastodon_(r%C3%A9seau_social)) écrit en python et utilisable dans un [terminal](https://fr.wikipedia.org/wiki/%C3%89mulateur_de_terminal).

Si vous ne connaissez pas Mastodon, il s'agit d'un réseau social acentré (ou décentralisé) fonctionnant peu ou prou sur le même principe que Twitter, à la différence près que vous disposez de plusieurs instances différentes, et que [vous pouvez choisir où vous souhaitez vous inscrire](https://joinmastodon.org) (un peu comme l'e-mail, en fait).

Pour tous les détails sur l'installation et l'utilisation, merci de [vous reporter au wiki](http://wiki.art-software.fr/doku.php?id=dev:python:tootstream).
