#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import json
import os

#################################################################################
#               Exceptions                                                      #
#################################################################################
class SubstException(Exception):
    def __init__(self, *args, **kwargs):
        if len(args) > 0:
            err, args = args[0], args[1:]
        if err:
            print("SUBST Error: {}".format(err))
        else:
            print("SUBST ERROR")
        super().__init__(*args, **kwargs)

class SubstCfgNotFound(SubstException):
    def __init__(self, *args, **kwargs):
        super().__init__("configuration file not found", *args, **kwargs)

class SubstCfgNotSaved(SubstException):
    def __init__(self, *args, **kwargs):
        super().__init__("configuration file cannot be saved", *args, **kwargs)

#################################################################################
#               Cfg handling                                                    #
#################################################################################
def loadCfg(filepath):
    """Loads a substitution database from a file.
    filepath: The json file to load.
    Returns the dictionnary object."""
    if not (os.path.exists(filepath) and os.path.isfile(filepath)):
        raise SubstCfgNotFound
    with open(filepath, "r") as f:
        data = json.load(f)
    return data

def saveCfg(database, filepath):
    """Saves a substitution database (a dict object) to a json file on the disk.
    database: The dictionnary object to save ;
    filepath: The file to create."""
    try:
        with  open(filepath, "w") as f:
            json.dump(database, f)
    except PermissionError:
        raise SubstCfgNotSaved from PermissionError

#################################################################################
#               Main                                                            #
#################################################################################
def lookup(text, database):
    for k in database:
        if text.find(k) >= 0:
            text = text.replace(k, database[k])
    return text
