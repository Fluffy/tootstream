# -*- coding: UTF-8 -*-

import click
import configparser
import dateutil.parser
import getpass
import os
import random
import re
import readline
import subprocess
import sys
import threading
import time

from collections import OrderedDict, deque
from colored import fg, bg, attr, stylize
from mastodon import Mastodon, StreamListener
from mastodon.Mastodon import MastodonAPIError, MastodonNetworkError, MastodonIllegalArgumentError

from .toot_parser import TootParser
from . import substitutions

COLORS = ['red', 'green', 'yellow', 'blue', 'magenta', 'cyan', 'white']
STYLE= {
    'blocked':          fg('red') + attr('bold'),
    'boosttoot':        fg('green') + bg('red'),
    'boostusername':    fg('dark_gray'),
    'counters':         fg('cyan'),
    'display_name':     fg(34) + attr('bold'),
    'error':            fg('white') + bg('red'),
    'favtoot':          fg('red') + bg('yellow'),
    'follow':           fg('yellow') + attr('bold'),
    'hashtag':          fg(105) + attr('bold'),
    'instance':         fg('green') + attr('bold'),
    'mention':          fg('yellow') + attr('bold'),
    'notefav':          fg('green'),
    'notefollow':       fg('red') + bg('green'),
    'notereblog':       fg('yellow'),
    'newtoot':          fg('magenta') + attr('bold'),
    'prompt':           fg('white') + attr('bold'),
    'pruneinfo':        fg('yellow') + attr('bold'),
    'readsep':          fg('magenta'),
    'replyprev':        fg(73),
    'repnewtoot':       fg('red') + bg('yellow'),
    'spoilertext':      attr('underlined'),
    'timestamp':        fg('dark_gray'),
    'threadcur':        fg('cyan'),
    'threadnew':        fg('green'),
    'threadold':        fg('red'),
    'threadcurspoiler': fg('cyan') + attr('underlined'),
    'tootid':           fg('red') + attr('bold'),
    'tootunavail':      fg('dark_gray'),
    'tooturl':          fg('dark_gray') + attr("underlined") + '\x1B[3m',
    'pin':              fg("yellow"),
    'quote':            fg('dark_gray') + '\x1B[3m',
    'unblocked':        fg('red') + attr('bold'),
    'unboosttoot':      fg('red') + bg('green'),
    'unfav':            fg('yellow') + bg('red'),
    'unfollow':         fg('yellow') + attr('bold'),
    'url':              fg(75) + attr('underlined'),
    'urlid':            fg('red') + attr('bold'),
    'urlopentext':      fg('cyan'),
    'userid':           fg(73),
    'username':         fg('yellow'),
    'userurl':          fg('dark_gray')
}
default_rc = """
"\eOQ":"pin fetch\C-m"			# F2
"\eOR":"pin read:40 less\C-m"   	# F3
"\eOS":"\C-lpin read:3\C-m"		# F4

"\e[15~":"local 40 less\C-m"	        # F5
"\e[17~":"note 40 less\C-m"		# F6
"\e[18~":"home 40 less\C-m"		# F7
"\e[19~":"public 40 less\C-m"	        # F8

"\e[20~":"\C-llocal 3\C-m"		# F9
"\e[21~":"\C-lnote 3\C-m"		# F10
"\e[23~":"\C-lhome 3\C-m"		# F11
"\e[24~":"\C-lpublic 3\C-m"		# F12

"\C-e":"home 1\C-m"			# ^e
"\C-y":"local 1\C-m"			# ^x
"\C-o":"note 1\C-m"			# ^o
"\C-p":"queue\C-m"			# ^p
"\C-a":"public 1\C-m"			# ^a

"\C-s":"home display=y\C-m"		# ^s
"\C-u":"home display=n\C-m"		# ^u
"""


####################################################################################################
#               Global information structures                                                      #
####################################################################################################
class IdDict:
    """Represents a mapping of local (tootstream) ID's to global
    (mastodon) IDs."""
    def __init__(self):
        self._map = []

    def to_local(self, global_id):
        """Returns the local ID for a global ID"""

        global_id = int(global_id) # In case a string gets passed
        try:
            return "%X" % self._map.index(global_id)
        except ValueError:
            self._map.append(global_id)
            return "%X" % (len(self._map) - 1)

    def to_global(self, local_id):
        """Returns the global ID for a local ID, or None if ID is invalid.
        Also prints an error message"""
        try:
            local_id = int(local_id, base=16)
            return self._map[local_id]
        except (ValueError, IndexError):
            printError("Invalid ID")
            return None

class TootListener(StreamListener):
    def __init__(self, mastodon, tl="home"):
        super().__init__()
        self.mastodon = mastodon
        self.tl = tl

    def on_update(self, status):
        global Unread, AutoShare, PinnedTags, AutoDisplay, Showed

        if self.tl in Unread.keys():
            if type(status["created_at"]) == str:
                status["created_at"] = dateutil.parser.parse(status["created_at"])

            if ("@"+status["account"]["acct"]) in AutoShare and \
                status["visibility"]!="private" and \
                not status["account"]["locked"]:
                if status["reblog"]:
                    self.mastodon.status_reblog(status["reblog"]["id"])
                else:
                    self.mastodon.status_reblog(status["id"])
            if self.tl in AutoDisplay and "id" in status and \
                not (status["id"] in Showed or
                    (status["reblog"] and status["reblog"]["id"] in Showed)
                ):
                    printToot(status, mastodon=self.mastodon)
            else:
                Unread[self.tl].appendleft(status)

    def on_notification(self, note):
        global Unread, AutoDisplay, Showed

        if self.tl in AutoDisplay and "id" in note and not note["id"] in Showed_Notif:
            printNoteList([note], self.mastodon)
        else:
            Unread["notes"].appendleft(note)

    def on_delete(self, status_id):
        global Unread
        if type(status_id) == str:
            status_id = int(status_id)

        if self.tl in Unread.keys():
            for toot in filter(lambda t: t["id"] == status_id, Unread[self.tl].copy()):
                Unread[self.tl].remove(toot)

def updateThread(mastodon, timeline="home"):
    tlStreamListener = TootListener(mastodon, timeline)
    t = time.time()
    waitTime = 1
    while True:
        try:
            if timeline == "home":
                mastodon.stream_user(tlStreamListener)
            elif timeline == "public":
                mastodon.stream_public(tlStreamListener)
            elif timeline == "local":
                mastodon.stream_local(tlStreamListener)
            elif timeline == "pin":
                while True:
                    pin(mastodon, "fetch")
                    time.sleep(3600)
        except KeyboardInterrupt:
            exit(0)
        except:
            t2 = time.time()
            if t2-t > 1+(2**waitTime):
                waitTime = 1
            elif waitTime :
                waitTime += 1

            cprint("Listener died, relaunching in {} s  {} ".format(2**waitTime, "⠇⠋⠙⠸⢰⣠⣄⡆"[waitTime%8]), fg("white")+bg("red"))
            t = t2
            time.sleep(2**waitTime)

IDS = IdDict();

Showed       = []
Showed_Notif = []
Names = []
Substs = {}
Unread = {'home': deque(), 'public': deque(), 'local': deque(), 'pin': deque(), 'notes': deque()}
Threads = {'home': None, 'public': None, "local": None, "pin": None}
URLs = deque(maxlen=20)
PinnedTags = []
AutoShare=[]
AutoDisplay=[]
pattern = {
    "hashtag": re.compile(r'(#\w+)'),
    "mention": re.compile(r'(@\w+)'),
    "url": re.compile(r'((https?|s?ftp|magnet):(//)?\S+),?'),
    "quote": re.compile(r'(^|\n[  ]*)(«[^»]+?»|"[^"]+?")'),
    "ansiEsc": re.compile(r'[^a-zA-Z]+[a-zA-Z]'),
    "args": re.compile(r'"([^"]*)"|\'([^\']*)\'|(\S+)| -- (.*)')
}

toot_parser = TootParser(indent='  ')

class Completer:
    def complete(self, text, index):
        if index==0:
            self.matches = [item for item in list(commands.keys())+Names+list(Substs.keys()) if len(text)==0 or item.find(text)==0]
        rep = None
        if index<=len(self.matches):
            rep = self.matches[index]
        return rep

####################################################################################################
#               Config / login                                                                     #
####################################################################################################
def parse_config(filename):
    (dirpath, basename) = os.path.split(filename)
    if not (dirpath == "" or os.path.exists(dirpath)):
        os.makedirs(dirpath)

    if not os.path.isfile(filename):
        return {}

    config = configparser.ConfigParser()

    parsed = config.read(filename)
    if len(parsed) == 0:
        return {}

    return config

def save_config(filename, instance, client_id, client_secret, token):
    (dirpath, basename) = os.path.split(filename)
    if not (dirpath == "" or os.path.exists(dirpath)):
        os.makedirs(dirpath)
    config = configparser.ConfigParser()
    config['default'] = {
        'instance': instance,
        'client_id': client_id,
        'client_secret': client_secret,
        'token': token
    }

    with open(filename, 'w') as configfile:
        config.write(configfile)



def login(mastodon, instance, email, password):
    """
    Login to a Mastodon instance.
    Return a Mastodon client if login success, otherwise returns None.
    """

    return mastodon.log_in(email, password)

####################################################################################################
#               Visual formatting tools                                                            #
####################################################################################################
def cprint(text, style, end="\n"):
    print(stylize(text, style), end=end)

def humanTime(ts):
    """Converts an ISO time string in human format"""
    tsNow = time.time()
    tsDiff = tsNow - ts
    if tsDiff<10:
        return "%s (Just now)" % time.strftime("%T", time.localtime(ts))
    elif tsDiff<120:
        return "%s (%d0 seconds ago)" % (time.strftime("%T", time.localtime(ts)), int(tsDiff//10))
    elif tsDiff<1200:
        return "%s (%d minutes ago)" % (time.strftime("%T", time.localtime(ts)), int(tsDiff//60))
    elif tsDiff<3600:
        return "%s (%d0 minutes ago)" % (time.strftime("%R", time.localtime(ts)), int(tsDiff//600))
    elif tsDiff<86400:      #24 hours
        return "%s (%d hours ago)" % (time.strftime("%R", time.localtime(ts)), int(tsDiff//3600))
    elif tsDiff<2592000:    # 30 days ~ a month
        return "%s (%d days ago)" % (time.strftime("%A %-d %B, %R", time.localtime(ts)), int(tsDiff//86400))
    else:
        return "%s (%d months ago)" % (time.strftime("%A %-d %B, %R", time.localtime(ts)), int(tsDiff//2592000))

def pagedOutput(data):
    """Prints data using pager
    You can set it by using $PAGER environment variable ; you can pass optionnally arguments to it too"""
    pager = ["/usr/bin/less", "-crf"]
    has_tmux = False

    if 'PAGER' in os.environ:
        pager = os.environ["PAGER"].split(" ")

    # Pager path needs to be absolute for subprocess.call() to properly work
    if pager[0][0]!="/":
        for p in os.environ["PATH"].split(":"):
            if os.path.exists(os.path.join(p, pager[0].split("/")[-1])):
                pager[0] = os.path.join(p, pager[0].split("/")[-1])
                break

    if 'TMUX' in os.environ:
        has_tmux = True

    # Using pager in another TMUX tab is way more useful when available
    # because it allows one to continue working on something else on the primary
    # while retaining toots to be read on the other one.
    #
    # Plus you can stack any number of tabs this way, PAGER will keep the buffers
    # open
    #
    # If we can't well… Then we open directly the pager here, handing stdin over after
    # filling it with data to let user interact with it (arrows, search, quit etc)
    if has_tmux and os.path.exists("/usr/bin/tmux"):
        tmp_file_name = os.path.join("/tmp", "tootstream.{}.log".format(os.getpid()))
        with open(tmp_file_name, "w") as f:
            f.write("\n" + data)
        subprocess.call(["/usr/bin/tmux", "new-window", "-n", "page({})".format(os.getpid()), "--"] + pager + [os.path.join("/tmp", "tootstream.{}.log".format(os.getpid()))])
    else:
        if not os.path.exists(pager[0]):
            if has_tmux:
                printError("$TMUX set but binary /usr/bin/tmux not found ; $PAGER '{}' not found either".format(pager[0]))
            else:
                printError("$PAGER '{}' not found".format(pager[0]))
            return

        lproc = subprocess.Popen(pager, stdin=subprocess.PIPE)
        lproc.stdin.write(data.encode())
        lproc.stdin.flush()
        lproc.stdin = sys.stdin
        lproc.wait()

def printToot(toot, mastodon=None, isHistory=False, isCurrent=False, stdout=True, linePrefix="", prefixLen=0):
    """Prints a toot.
    mastodon : Mastodon instance, useless in history-type timeline (as it is used only to retrieve replies content)
    isHistory: Printing toot in an history timeline (as thread does), will not retrieve replies content as the history does it instead.
    isCurrent: In history mode, 'current' toot's content is colored in cyan."""
    global Showed, Names, URLs

    if not isHistory and mastodon==None:
        printError("Mastodon instance must be given to printToot if toot is not an history-type timeline")
        return

    #tootBuffer = "  {p}\n".format(p=linePrefix)
    tootBuffer = ""

    if int(toot["id"]) not in Showed:
        Showed.append(int(toot['id']))

    if type(toot["created_at"]) == str:
        toot["created_at"] = dateutil.parser.parse(toot["created_at"])

    if not isHistory and toot["reblog"]:
        tootBuffer += stylize(
            "  {p}@{handle} (id:{uid}) boosted on {ts}:\n".format(
                p       = linePrefix,
                handle  = toot["account"]["acct"],
                uid     = int(toot["account"]["id"]),
                ts      = humanTime(toot["created_at"].timestamp())
            ), STYLE["boostusername"]
        )
        if ("@%s"%toot["account"]["acct"]) not in Names:
            Names.append("@%s" % toot["account"]["acct"])
        toot = toot["reblog"]
        if type(toot["created_at"]) == str:
            toot["created_at"] = dateutil.parser.parse(toot["created_at"])
        if int(toot["id"]) not in Showed:
            Showed.append(int(toot["id"]))
    
    tootBuffer += "  {p}{display} – {handle} {uid}\n  {p}{counts} {tid}\t{vis}  {ts}\n".format(
            p       = linePrefix,
            display = stylize(toot["account"]["display_name"], STYLE["display_name"]),
            handle  = stylize("@"+toot["account"]["acct"], STYLE["username"]),
            uid     = stylize("(id:%d)" % int(toot['account']['id']), STYLE['userid']),
            counts  = stylize("🔁:{}  ★:{}".format(toot["reblogs_count"], toot["favourites_count"]), STYLE['counters']),
            tid     = stylize(IDS.to_local(toot["id"]), STYLE['tootid']),
            vis     = {"public": "\U0001F30D", "unlisted": "\U0001F513", "private": "\U0001F512", "direct": "\u2709"}[toot["visibility"]],
            ts      = stylize(humanTime(toot['created_at'].timestamp()), STYLE['timestamp'])
    )

    if ("@%s"%toot["account"]["acct"]) not in Names:
        Names.append("@%s" % toot["account"]["acct"])

    Names += ["@%s" % mu["acct"] for mu in toot["mentions"] if ("@%s" % mu["acct"]) not in Names]


    # It's a reply to another toot => Write it for context
    if not isHistory and toot['in_reply_to_id']:
        try:
            repliedToot = mastodon.status(toot['in_reply_to_id'])
            tootBuffer += "  {p}{repuid}:\n  {p}{c}\n".format(
                    p=linePrefix,
                    repuid=stylize("In reply to @{}".format(repliedToot["account"]["acct"]), STYLE["replyprev"]),
                    c = stylize(get_content(repliedToot, colors=False, prefix_size=prefixLen)[2:].replace("\n  ", "\n  "+linePrefix), STYLE["replyprev"])
            )
            if ("@%s"%repliedToot["account"]["acct"]) not in Names:
                Names.append("@%s" % repliedToot["account"]["acct"])
        except MastodonAPIError:
            tootBuffer += stylize("  {p}(Previous toot unavailable)\n".format(p=linePrefix), STYLE["tootunavail"])

    # Show spoiler text, if any
    if toot['spoiler_text']!='':
        if isHistory and isCurrent:
            tootBuffer += "  {p}{spoilers}\n".format(
                    p        = linePrefix,
                    spoilers = stylize(stylize(toot['spoiler_text'], STYLE['spoilertext']), STYLE["threadcur"])
            )
        else:
            tootBuffer += "  {p}{spoilers}\n  {p}\n".format(
                    p = linePrefix,
                    spoilers = stylize(toot['spoiler_text'], STYLE['spoilertext'])
            )

    # toot content, pre-aligned in get_content
    if isHistory and isCurrent:
        tootBuffer += "  {p}{c}\n".format(
                p = attr("reset") + linePrefix + STYLE["threadcur"],
                c = stylize(get_content(toot, colors=False, prefix_size=prefixLen)[2:].replace("\n  ", "\n  "+attr("reset") + linePrefix + STYLE["threadcur"]), STYLE["threadcur"])
        )
    else:
        tootBuffer += "  {p}{c}\n".format(
                p = linePrefix,
                c = get_content(toot, prefix_size=prefixLen)[2:].replace("\n  ", "\n  "+linePrefix)
        )

    if toot["media_attachments"]:
        tootBuffer += "  " + linePrefix + "\n  " + linePrefix + stylize("medias:", attr("underlined"))
        for m in toot["media_attachments"]:
            url = m["url"]
            if m["remote_url"] is not None and len(m["remote_url"])>0:
                url = m["remote_url"]
            if "description" in m and m["description"] != None:
                description = m["description"] + "\n"
            else:
                description = ""
            tootBuffer += "\n  {p}‣ ".format(p=linePrefix) + description + stylize(url, STYLE["url"])
            if url in URLs:
                URLs.remove(url)
            URLs.appendleft(url)
        tootBuffer += "\n"

    tootBuffer += "  " + linePrefix + stylize(toot["url"], STYLE["tooturl"]) + "\n"

    if not stdout:
        return tootBuffer
    print(tootBuffer)
    return None

def printNoteList(notes, mastodon, showAll=False, stdout=True):
    global Showed_Notif, Names
    outBuffer = ""
    for note in notes:
        if not int(note['id']) in Showed_Notif or showAll:
            display_name = "  " + note['account']['display_name']
            username = " @" + note['account']['acct']

            if ("@%s" % username) not in Names:
                Names.append("@%s" % username)

            # Mentions
            if note['type'] == 'mention':
                buf = printToot(note['status'], mastodon, stdout=False)
                outBuffer += buf

            # Favorites
            elif note['type'] == 'favourite':
                reblogs_count = "  " + "🔁:" + str(note['status']['reblogs_count'])
                favourites_count = " ★:" + str(note['status']['favourites_count'])
                time = " " + humanTime(note['status']['created_at'].timestamp())
                content = get_content(note['status'], colors=False)
                outBuffer += stylize(display_name + username + " favorited your status:", STYLE['notefav']) + "\n" + \
                    stylize(reblogs_count + favourites_count + time + '\n' + content, STYLE['notefav']) + "\n"

            # Boosts
            elif note['type'] == 'reblog':
                outBuffer += stylize(display_name + username + " boosted your status:", STYLE['notereblog']) + "\n" + \
                    stylize(get_content(note['status'], colors=False), STYLE['notereblog']) + "\n"

            # Follows
            elif note['type'] == 'follow':
                username = re.sub('<[^<]+?>', '', username)
                display_name = note['account']['display_name']
                outBuffer += "  " + stylize(display_name + username + " followed you!", STYLE['notefollow']) + "\n"

            if int(note['id']) not in Showed_Notif:
                Showed_Notif.append(int(note['id']))
            outBuffer += "\n"
    if not stdout:
        return outBuffer+"\n"
    else:
        print(outBuffer)

def printUserInfo(u):
    """Prints user info"""
    print(stylize(u['display_name'], STYLE['display_name']) + " " +
          stylize("@%s" % u['acct'], STYLE['username']) + " " +
          stylize("id: " + str(u['id']), STYLE['userid']) + "\n" +
          stylize(u['url'], STYLE['userurl']) + "\n" +
          "Avatar: " + stylize(u["avatar"], STYLE["userurl"]) + "\n" +
          "Header: " + stylize(u["header"], STYLE["userurl"]) + "\n" +
          stylize("{} statuses, {} subscriptions, {} followers".format(u["statuses_count"], u["following_count"], u["followers_count"]), STYLE["counters"]) + "\n" +
          get_content({'content': u['note']}) + "\n")

def printError(text):
    """Prints error text."""
    cprint("ERROR: {}".format(text), STYLE["error"])



def getUsers(mastodon, handlename):
    """Gets an user dict list by looking up their name"""
    result = None

    try:
        result = mastodon.account_search(handlename)
    except MastodonAPIError:
        printError("user {} not found".format(rest))
    return result

def cutText(text, width, stripSpaces = True):
    """Cuts text at a given width on spaces"""
    for line in text.replace("	", "        ").split("\n"):
        if stripSpaces:
            line = line.strip()
        escSize = len(''.join(pattern["ansiEsc"].findall(line)))
        while len(line)-escSize>width and line.find(" ")>0 and line.find(" ")<width+escSize:
            cutPos = line[:width].rfind(" ")
            yield line[:cutPos]
            line = line[cutPos+1:]
        yield line

def get_content(toot, colors=True, prefix_size=0):
    global pattern
    if toot is None:
        toot = {'content': ''}

    html = toot['content']
    if html is None:
        html = ''
    toot_parser.reset()
    toot_parser.feed(html)
    toot_parser.close()

    t = "\n"+toot_parser.get_text()
    for url in [i[0] for i in pattern["url"].findall(t)]:
        if url in URLs:
            URLs.remove(url)
        URLs.appendleft(url)

    for quote_start, quote in pattern["quote"].findall(t):
        quote_out = quote_start + "> " + quote[1:-1].strip().replace("\n", "\n> ") + "\n"
        t = t.replace(quote, quote_out).replace(quote_out+"\n", quote_out)

    try:
        w, _ = os.get_terminal_size()
    except OSError:
        w = 80

    # Quotes printing
    t_out = ""
    for t_line in t.lstrip().split("\n"):
        isQuote = False
        if t_line.lstrip().startswith("> "):
            t_line = t_line.lstrip()[2:]
            isQuote = True
        t_line = [i+"\n" for i in cutText(t_line, w-4-prefix_size)]
        if isQuote:
            t_out += "  │ " + "  │ ".join([stylize(t_line_subline, STYLE["quote"]) for t_line_subline in t_line])
        else:
            t_out += "  " + "  ".join(t_line)
    t = t_out
    if t_out[-1] == "\n":
        t = t_out[:-1]

    if colors:
        t = pattern["url"].sub(stylize(r'\1', STYLE['url']), t)
        t = pattern["hashtag"].sub(stylize(r'\1', STYLE['hashtag']), t)
        t = pattern["mention"].sub(stylize(r'\1', STYLE['mention']), t)
    return t

####################################################################################################
#               User-side commands                                                                 #
####################################################################################################
commands = OrderedDict()


def command(func):
    commands[func.__name__] = func
    return func


@command
def help(mastodon, *args):
    """Displays help for a command or list all of them.
    help [command] - Get help for a precise command"""
    if len(args)<1:
        print("Commands:")
        for command, cmd_func in sorted(commands.items()):
            print("    {}\t{}".format(command+("\t" * (len(command)<4)), cmd_func.__doc__.split("\n")[0]))
    else:
        for arg in args:
            if arg in commands:
                helpText = commands[arg].__doc__
                print("  {}\n    {}".format(arg, helpText))

#-------------- Toot interaction -------------------------------------------------------------------
@command
def queue(mastodon, *args):
    """Prints info about or manages the toot queues
    queue [print] - Print queues size
    queue prune:<public|home|local|pin>[:n]- purges a queue, optionnaly removing n toots only"""
    global Unread
    printQueue = True
    
    args = list(args)
    for arg in args.copy():
        params = arg.split(":", 1)
        if params[0] == "prune":
            if len(params) == 2:
                params = params[1].split(":")
                if len(params) == 1:
                    size = -1
                else:
                    size = int(params[1])
                queueName = params[0]
                if queueName not in ["public", "local", "home", "pin"]:
                    printError("Queue name invalid")
                    return
                if size == -1:
                    size = len(Unread[queueName])
                size = min(size, len(Unread[queueName]))

                for k in range(size):
                    _ = Unread[queueName].pop()
                cprint("> Removed {} toots in queue".format(size), STYLE['pruneinfo'])
                printQueue = False
            args.remove(arg)
        elif params[0] == "print":
            printQueue = True
            args.remove(arg)

    if printQueue:
        for tl in Unread.keys():
            tootNum = len(Unread[tl])
            print("{} queue: ".format(tl), end="")
            if tootNum == 0:
                print("<empty>")
            else:
                print("{} unread toots".format(tootNum))

@command
def toot(mastodon, *args):
    """Publishes a toot.
    toot [cw] [vis:<private|public|unlisted|direct>] [rep:<id>] [media:<local file path>] [sensitive] [--] <text>

    To post multiple toots at a time, use "---" on a single line to cut.
    Toots will be made into a thread.

    cw: Content warning, uses first line as a header.
    vis: Visibility of toot
    rep: Toot id to reply to
    media: Post a media (can be repeated)
    sensitive: Marks medias as sensitive"""
    global Substs, IDS
    tootArgs = {
        "sensitive": False,
        "visibility": "public",
        "spoiler_text": None,
        "media_ids": None
    }
    mentions_pre = ""
    mentions_post = ""

    args = list(args)
    for arg in args.copy():
        params = arg.split(":", 1)
        if params[0] == "cw":
            tootArgs["sensitive"] = True
            args.remove(arg)
        elif params[0] == "vis":
            if len(params) == 1:
                printError("Need visibility !")
                return
            elif params[1] not in ["private", "public", "unlisted", "direct"]:
                printError("Visibility not one of the following: private, public, unlisted or direct")
                return
            tootArgs["visibility"] = params[1]
            args.remove(arg)
        elif params[0] == "rep":
            if len(params) < 2:
                printError("Need toot id to reply to !")
                return

            # set reply id
            try:
                tootArgs["in_reply_to_id"] = IDS.to_global(params[1])
            except ValueError:
                printError("toot id is invalid !")
                return

            pToot = mastodon.status(tootArgs["in_reply_to_id"])

            # match default visibility
            if tootArgs["visibility"] == "public":
                if pToot["visibility"] == "public":
                    tootArgs["visibility"] = "unlisted"
                else:
                    tootArgs["visibility"] = pToot["visibility"]

            ## set cw 
            tootArgs["sensitive"] = tootArgs["sensitive"] or pToot["sensitive"]
            # this will put the previous cw if there was no cw specified
            tootArgs["spoiler_text"] = pToot["spoiler_text"]

            # fill in mentions
            user = mastodon.account_verify_credentials()
            mentions = [i['acct'] for i in pToot['mentions']]
            mentions.append(pToot['account']['acct'])
            mentions = ["@%s" % i for i in list(set(mentions))] # Remove dups
            if ("@%s" % str(user['username'])) in mentions:
                mentions.remove("@%s" % str(user['username']))
            if "@"+pToot["account"]["acct"] in mentions:
                mentions_pre = "@" + pToot["account"]["acct"]
                mentions.remove("@%s" % pToot["account"]["acct"])
            mentions_post = ' '.join(mentions)
            args.remove(arg)
        elif params[0] == "media":
            m = None
            try:
                m = mastodon.media_post(params[1])
            except MastodonIllegalArgumentError:
                printError("Media could not be posted")
                return
            if m is None or 'id' not in m:
                printError("Media could not be posted")
                return
            if tootArgs["media_ids"] is None:
                tootArgs["media_ids"] = []
            if len(tootArgs["media_ids"]) >= 4:
                printError("Cannot exceed 4 medias in a single post, sorry.")
                return
            tootArgs["media_ids"].append(m)
            args.remove(arg)

    content = " ".join(args).replace("\\p", "\n\n").replace("\\n", "\n")

    if tootArgs["sensitive"]:
        content = content.split("\n", 1)

        if len(content) == 2:
            tootArgs["spoiler_text"], content = content[0], content[1]
        else:
            tootArgs["sensitive"] = False
            content = content[0]

    content = substitutions.lookup(content, Substs)

    try:
        if content.find("\n---\n")>=0:
            multiPost_media_deleted = False
            for part in content.split("\n---\n"):
                myToot = mastodon.status_post("{pre} {t} {post}".format(pre=mentions_pre, t=part, post=mentions_post), **tootArgs)
                tootArgs["in_reply_to_id"] = myToot["id"]
                if tootArgs["visibility"] == "public":
                    tootArgs["visibility"] = "unlisted"
                myToot["id"] = int(myToot["id"])
                printToot(myToot, mastodon, isHistory=True, isCurrent=False)
                if not multiPost_media_deleted and tootArgs["media_ids"] is not None and len(tootArgs["media_ids"]) > 0:
                    tootArgs["media_ids"] = None
                    multiPost_media_deleted = True
                time.sleep(2)
        else:
            myToot = mastodon.status_post("{pre} {t} {post}".format(pre=mentions_pre, t=content, post=mentions_post), **tootArgs)
            myToot["id"] = int(myToot["id"])
            cprint("You tooted:\n", STYLE['newtoot'], end="")
            printToot(myToot, mastodon)
    except MastodonAPIError:
        printError("Toot could not be posted.")

@command
def e(mastodon, *args):
    """Edits a toot with an external editor ($EDITOR)
    First line can be used to specify tags, by specifying the tags: argument (see help toot)
    Text can be multiline.

    e [args for toot]"""
    from random import randint
    from subprocess import call

    prog = os.environ.get("EDITOR", "/usr/bin/vim")
    progargs = ()
    randName = "/tmp/" + ''.join([chr(randint(ord('a'), ord('z'))) for _ in range(16)]) + '.txt'

    if "/" not in prog:
        for possiblePath in os.environ.get("PATH", "/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin").split(":"):
            if os.path.exists("{p}/{e}".format(p=possiblePath, e=prog)):
                prog = "{p}/{e}".format(p=possiblePath, e=prog)
                break
    if " " in prog:
        prog, *progargs = prog.split()
    if call([prog, *progargs, randName]) == 0:
        try:
            with open(randName, "r") as f:
                buff = f.read()
                if buff[-1] == "\n":
                    buff = buff[:-1]
            head = buff.split("\n")[0]
            if len(head)>0 and head.split(":")[0] == "tags":
                rest += " " + head
                _, buff = buff.split("\n", 1)
            toot(mastodon, *(args + (buff.replace("\n", "\\n").replace("\\n\\n","\\p"), )))
            os.unlink(randName)
        except FileNotFoundError:
            printError("File '{}' has not been found ; did you save it ?".format(randName))

@command
def delete(mastodon, *args):
    """Deletes your toot.
    delete <id>[ id […]]"""
    for tootid in args:
        tootid = IDS.to_global(tootid)
        if tootid is None:
            continue
        mastodon.status_delete(tootid)
        print("Poof! Toot #{} is gone.".format(tootid))

@command
def raw(mastodon, *args):
    """Prints a toot's fields
    raw <id>"""
    for tootid in args:
        tootid = IDS.to_global(tootid)
        if tootid is None:
            return
        toot = mastodon.status(tootid)
        if type(toot["created_at"]) != str:
            toot["created_at"] = toot["created_at"].strftime("%FT%T%z")
        toot["account"] = "\n\t" + "\n\t".join(["\033[1m{}: \033[0m{}".format(f, toot["account"][f]) for f in sorted(toot["account"].keys())])
        print("\n".join(["\033[1m{}: \033[0m{}".format(field, str(toot[field])) for field in sorted(toot.keys())]))

@command
def boost(mastodon, *args):
    """Boosts a toot by ID.
    boost <id>[ id […]]"""
    for tootid in args:
        tootid = IDS.to_global(tootid)
        if tootid is None:
            continue
        mastodon.status_reblog(tootid)
        boosted = mastodon.status(tootid)
        msg = "  Boosted: " + get_content(boosted)
        cprint(msg, STYLE['boosttoot'])

@command
def unboost(mastodon, *args):
    """Removes a boosted tweet by ID.
    unboost <id>[ id […]]"""
    for tootid in args:
        tootid = IDS.to_global(tootid)
        if tootid is None:
            continue
        mastodon.status_unreblog(tootid)
        unboosted = mastodon.status(tootid)
        msg = "  Removed boost: " + get_content(unboosted)
        cprint(msg, STYLE['unboosttoot'])

@command
def share(mastodon, *args):
    """Automatically shares someone's toots when they come
    share add <name> - Adds someone to the list
    share del <name> - Removes someone from the list
    share list - Lists all pinned users"""
    global AutoShare

    if len(args)<1:
        printError("You need an argument.")
        return

    if args[0] == "add" and len(args) == 2:
        if args[1] not in AutoShare:
            AutoShare.append(args[1])
            cprint("> {} added to auto share list".format(args[1]), fg("yellow"))
    elif args[0] == "del" and len(args) == 2:
        if args[1] in AutoShare:
            AutoShare.remove(args[1])
            cprint("> {} removed from auto share list".format(args[1]), fg("yellow"))
    elif args[0] == "list":
        print("Auto shared:")
        print("\n".join(["- "+name for name in sorted(AutoShare)]))
    else:
        printError("Invalid argument")

@command
def fav(mastodon, *args):
    """Favorites a toot by ID.
    fav <id>[ id[ …]]"""
    for tootid in args:
        tootid = IDS.to_global(tootid)
        if tootid is None:
            continue
        mastodon.status_favourite(tootid)
        faved = mastodon.status(tootid)
        msg = "  Favorited: " + get_content(faved)
        cprint(msg, STYLE['favtoot'])

@command
def unfav(mastodon, *args):
    """Removes a favorite toot by ID.
    unfav <id>[ id[ …]]"""
    for tootid in args:
        tootid = IDS.to_global(tootid)
        if tootid is None:
            continue
        mastodon.status_unfavourite(tootid)
        unfaved = mastodon.status(tootid)
        msg = "  Removed favorite: " + get_content(unfaved)
        cprint(msg, STYLE['unfav'])

#-------------- Timelines --------------------------------------------------------------------------
@command
def tl(mastodon, *args):
    """Displays a timeline.
    tl <home|public|local> [stream] [all] [n] [less] [display[=<yes|no>]]
        home|public|local: Shows home / public / local timeline.
        stream: Starts a streaming background thread
        all: shows all (read+unread) toots ;
        n: Optionnally show n toots at a time (default: 8)
        display: automatically displays toots on streaming timeline thread
        less: Outputs to a pager"""
    global Showed, Unread, IDS, tootListener, Threads, AutoDisplay
    w, _ = os.get_terminal_size()
    showRead = False
    unreadSize = 8
    startThread = False
    tl = None
    less = False

    for arg in args:
        if arg in ["public", "home", "local"]:
            tl = arg
        elif arg == "all":
            showRead = True
        elif arg == 'stream':
            startThread = True
        elif arg == "less":
            less = True
        elif arg.split("=", 1)[0]=="display":
            val = None
            if "=" in arg:
                arg, val = arg.split("=", 1)
            if (val in ["y", "Y", "yes", "YES"] or "=" not in arg) and tl is not None and tl not in AutoDisplay:
                AutoDisplay.append(tl)
                cprint("> {} toots are now automatically displayed".format(tl), fg("yellow"))
            elif val in ["n", "N", "no", "NO"] and tl is not None and tl in AutoDisplay:
                AutoDisplay.remove(tl)
                cprint("> {} toots are not automatically displayed anymore".format(tl), fg("yellow"))
            return
        else:
            try:
                unreadSize = int(arg)
            except ValueError:
                pass

    if tl == None:
        printError("no valid timeline given in home, local or public")
        return

    if startThread and tl in ["home", "public", "local"]:
        if Threads[tl] == None:
            Threads[tl] = threading.Thread(target=updateThread, args=(mastodon, tl, ), daemon=True)
            cprint('> Starting {} streaming thread'.format(tl), fg("yellow"))
            Threads[tl].start()
        else:
            printError("Thread already started")
        return

    try:
        if showRead or len(Showed)<10 or Threads[tl]==None:
            timeline = [toot for toot in reversed(mastodon.timeline(timeline=tl, limit=4096))] + list(Unread[tl])
        else:
            timeline = []
            while (len(timeline) < unreadSize) and len(Unread[tl])>0:
                t = Unread[tl].pop()
                if int(t["id"]) not in Showed:
                    timeline.append(t)

        read_items = [toot for toot in timeline if int(toot['id']) in Showed][-10:]
        unread_items = [toot for toot in timeline if int(toot['id']) not in Showed]

        if less:
            outBuffer = ''

        # 'read' part
        if showRead:
            for toot in read_items:
                try:
                    buf = printToot(toot, mastodon, stdout=not less)
                    if less:
                        outBuffer += buf+"\n"
                except MastodonAPIError:
                    if less:
                        outBuffer += stylize("  (toot unavailable)", STYLE["tootunavail"])+"\n"
                    else:
                        cprint("  (toot unavailable)", STYLE['tootunavail'])

        if less:
            outBuffer += stylize(" " + "─"*(w-2), STYLE['readsep']) + "\n"
        else:
            cprint(" " + "─"*(w-2), STYLE['readsep'])
        if len(unread_items)>0:
            for toot in unread_items[:unreadSize]:
                try:
                    buf = printToot(toot, mastodon, stdout=not less)
                    if less:
                        outBuffer += buf + "\n"
                except MastodonAPIError:
                    if less:
                        outBuffer += stylize("  (toot unavailable)", STYLE["tootunavail"])+"\n"
                    else:
                        cprint("  (toot unavailable)", STYLE['tootunavail'])
        if tl not in Threads or Threads[tl] == None:
            Unread[tl] = deque(unread_items[unreadSize:])
    except (TypeError, MastodonAPIError):
        printError("Timeline could not be downloaded from server")

    if less:
        pagedOutput(outBuffer)

@command
def home(mastodon, *args):
    """Displays the Home timeline.
    home [new [n]] [less]
        new: Only shows unread toots;
        n: Optionnally show n toots at a time (default: 8)
        less: Outputs to a pager"""
    tl(mastodon, *(("home", ) + args))

@command
def public(mastodon, *args):
    """Displays the Public timeline.
    public [new [n]] [less]
        new: Only shows unread toots;
        n: Optionnaly show n toots at a time (default: 8)
        less: Outputs to a pager"""
    tl(mastodon, *(("public", ) + args))

@command
def local(mastodon, *args):
    """Displays the Local timeline.
    local [new [n]] [less]
        new: Only shows unread toots;
        n: optionnaly show n toots at a time.
        less: Outputs to a pager"""
    tl(mastodon, *(("local", ) + args))

@command
def user(mastodon, *args):
    """Displays user's timeline.
    Defaults to self.
    user [id:<id>] [less]
    id: user id to lookup
    less: Outputs to a pager"""
    less = False
    u = None
    try:
        for arg in args:
            carg = ""
            c = arg.split(":", 1)
            if len(c) == 1:
                c = c[0]
            else:
                c, carg = c[0], c[1]
            if c == "id":
                u = mastodon.account(int(carg))
            elif c == "less":
                less = True
        if u is None:
            u = mastodon.account_verify_credentials()

        timeline = []
        try:
            timeline = [toot for toot in reversed(mastodon.account_statuses(u['id']))]
        except MastodonAPIError:
            pass

        if less:
            outBuffer = ''
        for toot in timeline:
            try:
                buf = printToot(toot, mastodon, stdout=not less)
                if less:
                    outBuffer += buf + "\n"
            except MastodonAPIError:
                if less:
                    outBuffer += stylize("  (toot unavailable)", STYLE["tootunavail"]) + "\n"
                else:
                    cprint("  (toot unavailable)", STYLE['tootunavail'])
        if less:
            pagedOutput(outBuffer)
    except (MastodonAPIError, ValueError):
        printError("user ID is invalid")

@command
def pin(mastodon, *args):
    """Manages pinned tags.
    pin read[:n] [less]- read latest toots published with pinned tags (optionally to a pager)
    pin fetch - Force fetching of new toots
    pin add:<taga[,tagb[,…]]> - pins one or more tag(s)
    pin del:<taga[,tagb[,…]]> - unpins one or more tag(s)
    pin list - List pinned tags
    pin stream - Start pin stream thread"""
    global Unread, PinnedTags, Showed
    doRead = False
    doFetch = False
    less = False
    readN = 4

    for arg in args:
        if arg[:4] == "read":
            doRead = True
            if len(arg)>4:
                try:
                    readN = int(arg.split(":")[1])
                except ValueError:
                    printError("Parameter following read: must be a number")
                    return
        elif arg == "fetch":
            doFetch = True
        elif arg[:3] == "add" and len(arg) > 4:
            for tag in arg.split(":", 1)[1].split(","):
                if tag not in PinnedTags:
                    PinnedTags.append(tag)
                    cprint("> Tag {} pinned".format(tag), STYLE["pin"])
            doFetch = True
        elif arg[:3] == "del" and len(arg) > 4:
            for tag in arg.split(":", 1)[1].split(","):
                if tag in PinnedTags:
                    PinnedTags.remove(tag)
                    cprint("> Tag {} unpinned".format(tag), STYLE["pin"])
        elif arg == "list":
            print("Pinned tags:")
            if len(PinnedTags) > 0:
                print("- " + "\n- ".join(sorted(PinnedTags)))
        elif arg == "stream":
            if Threads["pin"] == None:
                Threads["pin"] = threading.Thread(target=updateThread, args=(mastodon, "pin", ), daemon=True)
                cprint('> Starting pin streaming thread', fg("yellow"))
                Threads["pin"].start()
            else:
                printError("Thread already started")
            return
        elif arg == "less":
            less = True

    if doFetch:
        oldLength = len(Unread["pin"])
        for tagId, tag in enumerate(PinnedTags):
            print("\r> Fetching tagged toots… {}".format("⠇⠋⠙⠸⢰⣠⣄⡆"[tagId%8]), end="\x1B[0K")
            try:
                for toot in reversed(mastodon.timeline_hashtag(tag)):
                    if int(toot["id"]) not in Showed and toot["id"] not in [t["id"] for t in Unread["pin"]]:
                        if type(toot["created_at"]) == str:
                            toot["created_at"] = dateutil.parser.parse(toot["created_at"])
                        Unread["pin"].appendleft(toot)
            except MastodonAPIError:
                pass
        Unread["pin"] = deque([toot for *_, toot in reversed(sorted((toot["created_at"].timestamp(), toot["id"], toot) for toot in Unread["pin"]))])
        cprint("\r> Fetched {} toots\x1B[0K".format(len(Unread["pin"]) - oldLength), STYLE["pin"])

    if not doRead:
        return
    w, _ = os.get_terminal_size()

    if less:
        outBuffer = " " + stylize("—"*(w-2), STYLE["readsep"]) + "\n"
    else:
        cprint(" " + "─"*(w-2), STYLE['readsep'])
    if len(Unread["pin"])>0:
        for _ in range(min(len(Unread["pin"]), readN)):
            toot = Unread["pin"].pop()
            try:
                buf = printToot(toot, mastodon, stdout=not less)
                if less:
                    outBuffer += buf+"\n"
            except MastodonAPIError:
                if less:
                    outBuffer += stylize("  (toot unavailable)", STYLE["tootunavail"])+"\n"
                else:
                    cprint("  (toot unavailable)", STYLE['tootunavail'])
    if less:
        pagedOutput(outBuffer)

@command
def thread(mastodon, *args):
    """Displays the thread the toot belongs to.
    thread <id> [less]"""
    less = False
    toot_id = None
    if len(args) >=1:
        toot_id = IDS.to_global(args[0])
        if len(args) == 2:
            if args[1] == "less":
                less = True
    if toot_id is None:
        return
    try:
        dicts = mastodon.status_context(toot_id)
    except MastodonAPIError:
        printError("Thread not displayable")
        return

    # No history
    if ((len(dicts['ancestors']) == 0) and (len(dicts['descendants']) == 0)):
        cprint("No history to show.", STYLE['threadcur'])
        return

    message_pool = dicts["ancestors"]+dicts["descendants"]+[mastodon.status(toot_id)]
    message_pool_ids = [int(m["id"]) for m in message_pool]

    first_message = list(filter(lambda m: "in_reply_to_id" not in m or m["in_reply_to_id"] is None or int(m["in_reply_to_id"]) not in message_pool_ids, message_pool))
    if len(first_message) == 0:
        # could not retrieve first message.
        # we need to sort them by date, and take the first not refd'
        printError("Could not retrieve first message")
        if less:
            return ''
        return

    first_message = first_message[0]

    outBuffer = ""
    thread_lifo = [(0, first_message)]
    while len(thread_lifo) > 0:
        cur_lvl, cur_message = thread_lifo.pop()
        answers_list = [
                (cur_lvl+1, i) for _, i in reversed(
                    sorted(
                        (int(m["id"]), m) for m in filter(
                            lambda m: 'in_reply_to_id' in m and m["in_reply_to_id"] is not None and int(m["in_reply_to_id"])==int(cur_message["id"]),
                            message_pool
                        )
                    )
                )
            ]
        thread_lifo += answers_list
        linePrefix="".join((["\033[1;37m│", "\033[1;30m│"]*int(.5 + (cur_lvl/2)))[:cur_lvl])+"\033[0m"
        outBuffer += "  " + linePrefix+"\n"+printToot(cur_message, mastodon, isHistory=True, isCurrent=int(cur_message["id"])==int(toot_id), stdout=False, linePrefix=linePrefix, prefixLen=cur_lvl)
    if less:
        pagedOutput(outBuffer)
    else:
        print(outBuffer)

@command
def note(mastodon, *args):
    """Displays the Notifications timeline
    note [all] [less] [n]
        all: show all notifs (only unread notifs are shown by default)"""
    global Unread, Threads
    w, _ = os.get_terminal_size()
    less = False
    showAll = False
    num = 8

    for arg in args:
        if arg == "all":
            showAll = True
        elif arg == "less":
            less = True
        elif len(arg) > 0:
            try:
                num = int(arg)
            except ValueError:
                printError("Argument must be an int, please see help note.")
                return

    if less:
        outBuffer = stylize(" " + "─"*(w-2), STYLE['readsep'])+"\n"
    else:
        cprint(" " + "─"*(w-2), STYLE['readsep'])

    if "home" not in Threads or Threads["home"] is None:
        for note in mastodon.notifications():
            if note not in Unread["notes"]:
                Unread["notes"].appendleft(note)    

    noteList = []
    for _ in range(num):
        if len(Unread["notes"])<1:
            break
        noteList.append(Unread["notes"].pop())

    if less:
        outBuffer += printNoteList(reversed(noteList), mastodon, showAll, False)
    else:
        printNoteList(reversed(noteList), mastodon, showAll)

    if less:
        pagedOutput(outBuffer)

#-------------- Search -----------------------------------------------------------------------------
@command
def tag(mastodon, *args):
    """Displays a tag timeline.
    tag [all] [hashtag]"""
    global Showed
    showAll = False
    _tag = None

    for arg in args:
        if arg == "all":
            showAll = True
        else:
            _tag = arg
    
    w, _ = os.get_terminal_size()
    timeline = []
    try:
        timeline = [toot for toot in reversed(mastodon.timeline_hashtag(_tag))]
    except MastodonAPIError:
        pass

    cprint(" " + "─"*(w-2), STYLE['readsep'])
    for toot in timeline:
        try:
            if showAll or int(toot['id']) not in Showed:
                printToot(toot, mastodon)
        except MastodonAPIError:
            cprint("  (toot unavailable)", STYLE['tootunavail'])

@command
def search(mastodon, *args):
    """Search for content (hashtags, users and toots)
    search <query>"""
    try:
        if len(args)<1:
            return
        data = mastodon.search(q=args[0], resolve=True)

        if len(data["hashtags"])>0:
            cprint("========== Tags ===========", STYLE["hashtag"])
            for h in data["hashtags"]:
                print("  {}".format(stylize("#" + h, STYLE["hashtag"])))
            print("")

        if len(data["accounts"])>0:
            cprint("========== Users ==========", STYLE["username"])
            for u in data["accounts"]:
                printUserInfo(u)
            print("")

        if len(data["statuses"])>0:
            cprint("========== Toots ==========", STYLE["tootid"])
            for t in data["statuses"]:
                printToot(t, mastodon)
            print("")

    except MastodonAPIError:
        printError("Invalid search (API returned an error)")


@command
def followed(mastodon, *args):
    """Get the list of followed accounts by a given user.
    followed [id]"""
    u = None
    if len(args)<1:
        u = mastodon.account_verify_credentials()
    else:
        try:
            u = mastodon.account(int(args[0]))
        except (MastodonAPIError, ValueError):
            printError("user ID {} is invalid".format(args[0]))
            return
    followedAccounts = mastodon.account_following(u["id"])
    for user in followedAccounts:
        info(mastodon, user["id"])

@command
def followers(mastodon, *args):
    """Get the list of followers of a given user.
    followers [id]"""
    u = None
    if len(args)<1:
        u = mastodon.account_verify_credentials()
    else:
        try:
            u = mastodon.account(int(args[0]))
        except (MastodonAPIError, ValueError):
            printError("user ID {} is invalid".format(args[0]))
            return
    followersAccounts = mastodon.account_followers(u["id"])
    for user in followersAccounts:
        info(mastodon, user["id"])

@command
def info(mastodon, *args):
    """Prints basic user info.
    info [id]"""
    u = None
    if len(args)<1:
        u = mastodon.account_verify_credentials()
    else:
        try:
            u = mastodon.account(int(args[0]))
        except (MastodonAPIError, ValueError):
            printError("user Id {} is invalid".format(args[0]))
            return
    printUserInfo(u)


@command
def url(mastodon, *args):
    """Get last URLs or open them in the browser
    url [id[ id[ …]]]"""
    if len(args)<1:
        print("Last 20 URLs caught:")
        for idx, url in enumerate(URLs):
            print("{}: {}".format(stylize("{:2d}".format(idx), STYLE["urlid"]), stylize(url, STYLE["url"])))
        return

    for urlId in args:
        try:
            urlId = int(args[0])
        except ValueError:
            printError("invalid URL id: {}".format(rest))
            continue
        if urlId<0 or urlId>=len(URLs):
            printError("invalid URL id")
            continue

        url = URLs[urlId]
        cprint('Opening {}…'.format(url), STYLE['urlopentext'])
        try:
            with open("/dev/null", "wb") as f:
                subprocess.call(['/usr/bin/xdg-open', url], stdout=f, stderr=f)
        except FileNotFoundError:
            printError("command 'xdg-open' was not installed. Aborting…")
            return

#-------------- User-related management ------------------------------------------------------------
@command
def block(mastodon, *args):
    """Blocks a user or domain, or give the block list.
    block - Prints the block list
    block user <id> - Blocks a given user
    block domain <domain> - Blocks a given domain"""

    if len(args)<1:
        print("User(s) currently blocked:")
        for u in mastodon.blocks():
            printUserInfo(u)
        try:
            print("Domain(s) currently blocked: \n" + "\n".join(["- {}".format(d) for d in mastodon.domain_blocks()]))
        except (TypeError, MastodonAPIError):
            printError("Mastodon could not retrieve the domain block list (this is probably a bug in Mastodon.py)")
        return
    try:
        if len(args) < 2:
            printError("Argument missing")
            return
        if args[0] == "user":
            u = mastodon.account(int(args[1]))
            mastodon.account_block(u['id'])
            cprint("> Blocked {} ({}).".format(u['display_name'], u['acct']), STYLE['blocked'])
        elif args[0] == "domain":
            mastodon.domain_block(args[1])
            cprint("> Blocked domain {}".format(args[1]), STYLE["blocked"])
    except (MastodonAPIError, ValueError):
        printError("user ID is invalid")

@command
def unblock(mastodon, *args):
    """Unblocks a user or a domain.
    unblock user <id> - Unblocks the given user
    unblock domain <domain> - Unblocks the given domain"""
    try:
        if len(args) < 2:
            printError("Argument missing")
            return
        if args[0] == "user":
            u = mastodon.account(int(args[1]))
            mastodon.account_unblock(u['id'])
            cprint("> Unblocked {} ({}).".format(u['display_name'], u['acct']), STYLE['unblocked'])
        elif args[0] == "domain":
            mastodon.domain_unblock(args[1])
            cprint("> Unblocked domain {}".format(args[1]), STYLE["unblocked"])
    except (MastodonAPIError, ValueError):
        printError("user Id is invalid")

@command
def follow(mastodon, *args):
    """Follows an account.
    follow <id>[ id[ …]]"""
    global Names
    for userid in args:
        try:
            u = mastodon.account(int(userid))
            mastodon.account_follow(u['id'])
            cprint("> Now following {} ({})".format(u['display_name'], u['acct']), STYLE['follow'])

            if ("@%s" % u["acct"]) not in Names:
                Names.append("@%s" % u["acct"])
        except (MastodonAPIError, ValueError):
            printError("user ID {} is invalid".format(userid))

@command
def unfollow(mastodon, *args):
    """Unfollows an account.
    unfollow <id>[ id[ …]]"""
    global Names
    for userid in args:
        try:
            u = mastodon.account(int(userid))
            me = mastodon.account_verify_credentials()
            mastodon.account_unfollow(u['id'])
            cprint("> You now don't follow {} ({}) anymore".format(u['display_name'], u['acct']), STYLE['unfollow'])

            if ("@%s" % u["acct"]) in Names:
                Names.remove("@%s" % u["acct"])
        except (MastodonAPIError, ValueError):
            printError("user ID {} is invalid".format(userid))

@command
def mute(mastodon, *args):
    """Mutes an account or give the mute list.
    mute [id[ id[ …]]]"""
    global Names
    if len(args)<1:
        print("User(s) currently muted:")
        for u in mastodon.mutes():
            printUserInfo(u)
        return
    for userid in args:
        try:
            u = mastodon.account(int(userid))
            mastodon.account_mute(u['id'])
            cprint("> {} ({}) has been muted".format(u['display_name'], u['acct']), STYLE['follow'])
            if ("@%s" % u["acct"]) not in Names:
                Names.append("@%s" % u["acct"])
        except (MastodonAPIError, ValueError):
            printError("user ID {} is invalid".format(userid))

@command
def unmute(mastodon, *args):
    """Unmutes an account.
    unmute <id>[ id[ …]]"""
    global Names
    for userid in args:
        try:
            u = mastodon.account(int(userid))
            mastodon.account_unmute(u['id'])
            cprint("> {} ({}) is not muted anymore".format(u['display_name'], u['acct']), STYLE['unfollow'])
            if ("@%s" % u["acct"]) in Names:
                Names.remove("@%s" % u["acct"])
        except (MastodonAPIError, ValueError):
            printError("user ID {} is invalid".format(userid))



#-------------- Administration ---------------------------------------------------------------------
@command
def report(mastodon, *args):
    """Reports some toots to the instance administrator.
    report <toot id[,toot_id[,…]] -- comment"""
    global IDS
    if len(args)<1:
        printError("Nothing to report")

    toot_ids = list(filter(lambda tid: tid is not None, [IDS.to_global(i) for i in args[0].split(",")]))
    comment = None
    if len(args) > 1:
        comment = ' '.join(args[1:]).strip()

    toot_ids_by_author = {}
    # We can only report one user at a time.
    # If you report toots from different authors, we need to sort them first
    # to report every toot you signaled from each user, one at a time.
    for toot_id in toot_ids:
        try:
            toot = mastodon.status(toot_id)
        except MastodonAPIError:
            continue

        toot_author_id = int(toot["account"]["id"])
        if toot_author_id not in toot_ids_by_author:
            toot_ids_by_author[toot_author_id] = []
        toot_ids_by_author[toot_author_id].append(toot_id)

    r_list = []
    for author_id in toot_ids_by_author.keys():
        r_list.append(mastodon.report(author_id, toot_ids_by_author[author_id], comment)["id"])

    print("> Issued {n} report(s) affecting {ntoot} toot(s) and {nuser} user(s), with comment: « {c} »\n> Report id(s): {ridlist}".format(
        n = len(r_list),
        c = comment,
        ntoot = sum([len(author_toots) for author_toots in toot_ids_by_author.values()]),
        nuser = len(toot_ids_by_author),
        ridlist = ", ".join([str(r) for r in r_list])
    ))



#-------------- Miscellaneous ----------------------------------------------------------------------
@command
def quit(mastodon, *args):
    """Ends the program."""
    sys.exit("Goodbye!")

@command
def update(mastodon, *args):
    """Update profile information
    update <note "text"|name "name">"""
    global Substs

    if len(args)<2:
        printError("Needs a command AND an argument (see help update)")
        return
    if args[0] not in ["note", "name"]:
        printError("{} : subcommand invalid (see help update)".format(args[0]))
        return

    u = None
    try:
        u = mastodon.account_verify_credentials()
    except MastodonAPIError:
        printError("Could not get user information")
        return
    try:
        if args[0] == "note":
            text = substitutions.lookup(args[1].replace("\\p", "\n\n").replace("\\n", "\n"), Substs)
            mastodon.account_update_credentials(note=text)
        elif args[0] == "name":
            mastodon.account_update_credentials(display_name=substitutions.lookup(args[1], Substs))
    except (MastodonAPIError, MastodonNetworkError):
        printError("Could not update info")

@command
def subst(mastodon, *args):
    """Manages substitution rules.
    subst add <string to replace> <string to replace to>
    subst del <string to replace>
    subst list"""
    global Substs
    cmd = args[0]
    data = ' '.join(args[1:])
    if cmd == "add":
        data = data.split(" ", 1)
        if len(data) == 1:
            printError("Substitution target text must be specified !")
            return
        Substs[data[0]] = data[1]
        substitutions.saveCfg(Substs, os.environ["HOME"] + "/.substdb")
    elif cmd == "del":
        if data == "":
            printError("Substitution target must be specified !")
            return
        if data in Substs.keys():
            del Substs[data]
            substitutions.saveCfg(Substs, os.environ["HOME"] + "/.substdb")
    elif cmd == "list":
        for k, i in sorted(Substs.items()):
            print("{}\t{}".format(k, i))
    else:
        printError("Parameter not understood.")

####################################################################################################
#               Main part                                                                          #
####################################################################################################

def authenticated(mastodon):
    if not os.path.isfile(APP_CRED):
        return False
    if mastodon.account_verify_credentials().get('error'):
        return False
    return True

def register_app(instance):
    return Mastodon.create_app(
        'tootstream',
        website='http://wiki.art-software.fr/doku.php?id=dev:python:tootstream',
        api_base_url="https://" + instance
    )

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])
@click.command(context_settings=CONTEXT_SETTINGS)
@click.option( '--instance', '-i', metavar='<string>',
               help='Hostname of the instance to connect' )
@click.option( '--email', '-e', metavar='<string>',
               help='Email to login' )
@click.option( '--password', '-p', metavar='<PASSWD>',
               help='Password to login (UNSAFE)' )
@click.option( '--config', '-c', metavar='<file>',
               type=click.Path(exists=False, readable=True),
               default='~/.config/tootstream/tootstream.conf',
               help='Location of alternate configuration file to load' )
def main(instance, email, password, config):
    global Names, Substs
    configpath = os.path.expanduser(config)
    config = parse_config(configpath)

    if 'default' not in config:
        config['default'] = {}

    if instance == None:
        if "instance" in config["default"]:
            instance = config["default"]["instance"]
        else:
            instance = input("Which instance would you like to connect to ? eg: 'mastodon.social' ")
            if instance == "":
                instance =  "mastodon.social"

    #----- Registering client -----
    try:
        client_id = None
        if "client_id" in config['default']:
            client_id = config['default']['client_id']

        client_secret = None
        if "client_secret" in config['default']:
            client_secret = config['default']['client_secret']

        if (client_id == None or client_secret == None):
            client_id, client_secret = register_app(instance)
    except MastodonNetworkError:
        printError("Unable to register tootstream as a client on {}. Look out for network errors and/or reach out your instance admin.".format(instance))
        exit(1)

    token = None
    if "token" in config['default']:
        token = config['default']['token']

    if (token == None or email != None or password != None):
        if (email == None):
            email = input("Welcome to tootstream! Two-Factor-Authentication is currently not supported. Email used to login: ")
        if (password == None):
            password = getpass.getpass()

        try:
            mastodon = Mastodon(
                client_id=client_id,
                client_secret=client_secret,
                api_base_url="https://" + instance
            )
            token = login(mastodon, instance, email, password)
        except MastodonIllegalArgumentError:
            printError("Invalid e-mail / password combination. Please retry or look out for network issues.")
            exit(2)

    mastodon = Mastodon(
        client_id=client_id,
        client_secret=client_secret,
        access_token=token,
        api_base_url="https://" + instance)

    save_config(configpath, instance, client_id, client_secret, token)

    say_error = lambda a, b: cprint("Invalid command. Use 'help' for a list of commands.",
            STYLE['error'])

    substFile = os.environ["HOME"] + "/.substdb"
    if not os.path.exists(substFile):
        with open(substFile, "w") as f:
            f.write("{}")
    Substs = substitutions.loadCfg(substFile)

    #----- Connected, running user command loop -----
    print("You are connected to {}.\nEnter a command. Use 'help' for a list of commands ; 'help <command>' for more info on any command.\n".format(stylize(instance, STYLE['instance'])))
    user = mastodon.account_verify_credentials()
    prompt = "\x01{}\x02[@{}]:\x01{}\x02\n".format(STYLE["prompt"], user['username'], attr("reset"))

    lastCommand = None

    # Fills the completer
    try:
        Names = ["@%s" % u["acct"] for u in mastodon.account_following(user["id"])]
        Names += ["@%s" % u["acct"] for u in mastodon.account_followers(user["id"]) if "@%s"%u["acct"] not in Names]
    except KeyError:
        pass # No follow{ed,ers}

    if os.path.exists("/etc/inputrc"):
        readline.read_init_file("/etc/inputrc")
    home = os.environ["HOME"]
    if os.path.exists(os.path.join(home, ".inputrc")):
        readline.read_init_file(os.path.join(home, ".inputrc"))
    if not os.path.exists(os.path.join(home, ".tootstream.inputrc")):
        with open(os.path.join(home, ".tootstream.inputrc"), "w") as f:
            f.write(default_rc)
    readline.read_init_file(os.path.join(home, ".tootstream.inputrc"))
    readline.set_completer(Completer().complete)
    readline.set_completer_delims("\t\n ")

    for tl in ["home", "local", "public"]:
        if Threads[tl] == None:
            Threads[tl] = threading.Thread(target=updateThread, args=(mastodon, tl, ), daemon=True)
            cprint('> Starting {} streaming thread'.format(tl), fg("yellow"))
            Threads[tl].start()

    while True:
        command = input(prompt)
        args = ""
        c = command.split(" ", 1)
        if len(c)>1:
            command, args = c[0], c[1]
        else:
            command = c[0]
        args = [list(j for j in i if j!='')[:1] for i in pattern["args"].findall(args)]
        args = [["", *i[:1]][len(i)>0] for i in args]
        if len(args)>=1 and args[0] == "--":
            args = args[1:]

        if command == "" and lastCommand != None:
            command, args = lastCommand
        lastCommand = [command, args]

        cmd_func = commands.get(command, say_error)
        try:
            cmd_func(mastodon, *args)
        except (MastodonAPIError, MastodonNetworkError):
            printError("Mastodon returned an error")

